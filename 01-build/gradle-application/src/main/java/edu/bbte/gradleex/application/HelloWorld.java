package edu.bbte.gradleex.application;

import java.util.logging.Logger;

public class HelloWorld {
    private static final Logger LOGGER = Logger.getLogger(HelloWorld.class.getName());

    public static void main(String[] args) {
        String name = System.getenv().getOrDefault("NAME", "stranger");
        LOGGER.info("Hello " + name);
    }
}
