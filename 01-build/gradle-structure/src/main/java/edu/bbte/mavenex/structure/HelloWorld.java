package edu.bbte.mavenex.structure;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;
import java.util.logging.Logger;

/**
 * példakód egyszerű állományolvasásra classpath-ből
 */
public class HelloWorld {
    private static final Logger LOGGER = Logger.getLogger(HelloWorld.class.getName());

    public static void main(String[] args) throws IOException {
        // nyitunk egy streamet a classpathből
        // nem feltétlenül egyezik a lokális fájlrendszer tartalmával
        InputStream propsStream = HelloWorld.class.getResourceAsStream("/hello.properties");

        // betöltjük a kulcs-érték párokat
        Properties props = new Properties();
        props.load(propsStream);

        // kulcs szerinti olvasás
        LOGGER.info("Hello " + props.get("name"));
    }
}
