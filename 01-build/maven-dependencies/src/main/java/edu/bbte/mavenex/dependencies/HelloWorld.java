package edu.bbte.mavenex.dependencies;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class HelloWorld {

    // logger betöltése
    // ez az osztály csak akkor látszik, ha van az slf4j-api-ra compile hatáskörű függőség
    public static final Logger LOG = LoggerFactory.getLogger(HelloWorld.class);

    public static void main(String[] args) throws IOException {
        new HelloWorld().logHelloMessage();
    }

    public void logHelloMessage() throws IOException {
        // logger használata
        String helloMessage = getHelloMessage();
        LOG.info(helloMessage);
    }

    public String getHelloMessage() throws IOException {
        InputStream propsStream = HelloWorld.class.getResourceAsStream("/hello.properties");
        Properties props = new Properties();
        props.load(propsStream);
        return "Hello " + props.getOrDefault("name", "stranger");
    }
}
