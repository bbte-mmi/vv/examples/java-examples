package edu.bbte.mavenex.dependencies;

import org.junit.jupiter.api.Test;

import java.io.IOException;

import static org.junit.jupiter.api.Assertions.assertEquals;

/**
 * Unit teszt példa
 */
public class HelloWorldTest {

    private final HelloWorld helloWorld = new HelloWorld();

    @Test
    public void testHelloMessage() throws IOException {
        // meghívjuk a tesztelendő kódot
        String helloMessage = helloWorld.getHelloMessage();
        // tesztelünk valamit vele kapcsolatban
        assertEquals("Hello Test User", helloMessage);
    }
}
