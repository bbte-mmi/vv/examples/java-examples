package edu.bbte.mavenex.structure;

import java.util.Properties;
import java.io.InputStream;
import java.io.IOException;

/**
 * példakód egyszerű állományolvasásra classpath-ből
 */
public class HelloWorld {
    public static void main(String[] args) throws IOException {
        // nyitunk egy streamet a classpathből
        // nem feltétlenül egyezik a lokális fájlrendszer tartalmával
        InputStream propsStream = HelloWorld.class.getResourceAsStream("/hello.properties");

        // betöltjük a kulcs-érték párokat
        Properties props = new Properties();
        props.load(propsStream);

        // kulcs szerinti olvasás
        System.out.println("Hello " + props.get("name"));
    }
}