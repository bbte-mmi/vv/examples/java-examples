package edu.bbte.gradleex.staticcodeanalysis;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Properties;
import java.io.InputStream;
import java.io.IOException;

public class HelloWorld {

    public static final Logger LOG = LoggerFactory.getLogger(HelloWorld.class);

    public static void main(String[] args) {
        new HelloWorld().logHello();
    }

    public void logHello() {
        try (InputStream propsStream = HelloWorld.class.getResourceAsStream("/hello.properties")) {
            Properties props = new Properties();
            props.load(propsStream);

            // logger használata
            LOG.info("Hello " + props.get("name"));

        } catch (IOException e) {
            LOG.error("Error reading stream", e);
        }
    }
}
