package edu.bbte.idde.example;

import edu.bbte.idde.example.model.BlogPost;
import edu.bbte.idde.example.repo.BlogPostDao;
import edu.bbte.idde.example.repo.DaoFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Date;

public class Main {
    private static final Logger LOG = LoggerFactory.getLogger(Main.class);

    public static void main(String[] args) {

        // DAO lekérése az abstract DAO factory mintával
        // NEM itt dől el az implementációs mód, vagy hogy hány példány jön létre
        DaoFactory daoFactory = DaoFactory.getInstance();
        BlogPostDao blogPostDao = daoFactory.getBlogPostDao();

        // DAO tesztelése
        blogPostDao.create(new BlogPost(1L, "Author 1", "Title 1", "Content 1", new Date()));
        blogPostDao.create(new BlogPost(2L, "Author 2", "Title 2", "Content 2", new Date()));
        blogPostDao.findAll().stream().forEach(blogPost -> LOG.info(blogPost.toString()));
    }
}
