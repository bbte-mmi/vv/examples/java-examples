package edu.bbte.idde.example.repo;

import edu.bbte.idde.example.repo.mem.MemDaoFactory;

/**
 * Általános factory
 */
public abstract class DaoFactory {
    // singleton lazy loading
    private static DaoFactory instance;

    /**
     * Kérünk egy példányt - itt dől el az adatelérési módszer.
     */
    public static synchronized DaoFactory getInstance() {
        if (instance == null) {
            instance = new MemDaoFactory();
        }
        return instance;
    }

    public abstract BlogPostDao getBlogPostDao();

    // További entitások DAOinak lekérése
}
