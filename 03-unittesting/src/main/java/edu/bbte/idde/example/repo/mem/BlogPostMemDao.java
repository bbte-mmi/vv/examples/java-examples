package edu.bbte.idde.example.repo.mem;

import edu.bbte.idde.example.model.BlogPost;
import edu.bbte.idde.example.repo.BlogPostDao;

import java.util.Collection;
import java.util.stream.Collectors;

/**
 * Teljes DAO megvalósítás egy entitásra:
 * - örökli a BlogPostDao-t az érintett entitás miatt
 * - örökli a MemDao-t az elérési mód miatt
 */
public class BlogPostMemDao extends MemDao<BlogPost> implements BlogPostDao {

    @Override
    public Collection<BlogPost> findByAuthor(String author) {
        return entities.values().stream()
            .filter(blogPost -> blogPost.getAuthor().equals(author))
            .collect(Collectors.toList());
    }
}
