package edu.bbte.idde.example.repo.mem;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class BlogPostMemDaoTest {

    private final BlogPostMemDao blogPostMemDao = new BlogPostMemDao();

    @Test
    void findByAuthorsTest() {
        var entities = blogPostMemDao.findAll();
        assertEquals(0, entities.size(), "Entities are not empty");
    }
}
